// require('dotenv').config();
import express from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";

import {connect_db} from "./server/db/mongoose.connection.js";
import user_router from "./server/modules/user.router.js";

import {error_handler,error_handler2,not_found} from "./server/middleware/errors.handler.js";   

const { PORT = 8080,HOST = "localhost", DB_URI } = process.env;

const app = express();

class Api{
  
  addingMiddlewares() {
    // middleware
    app.use(cors());
    app.use(morgan("dev"));
  }

  addingRoutes(){
    // routing
    // app.use("/api/stories", story_router);
    app.use("/api/users", user_router);
  }

  addingErrors() {
    // central error handling
    app.use(error_handler);
    app.use(error_handler2);
  }

  addingUse() {
    this.addingMiddlewares();
    this.addingRoutes();
    this.addingErrors();

    //when no routes were matched...
    app.use("*", not_found);
  }

  async connectToDB() {
    await connect_db(DB_URI as string);  
    await app.listen(Number(PORT),HOST);
  }

  async init() {
    this.addingUse();
    await this.connectToDB();
    log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
  }
}

const api = new Api();

api.init().catch(console.log);

